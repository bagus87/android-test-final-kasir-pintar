package com.example.kaspinfinaltes.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.kaspinfinaltes.model.BarangModel;
import com.example.kaspinfinaltes.model.StokModel;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    String TABLE_BARANG = "tb_barang";
    String TABLE_MANAJEMEN_STOK = "tb_management_stok";

    String KEY_ID = "id";
    String KEY_CODE_BARANG = "kode_barang";
    String KEY_DATE = "tanggal";
    String KEY_STOKIN = "stok_masuk";
    String KEY_PURCHASE_PRICE = "harga_jual";


    public DatabaseHelper(Context context) {
        super(context, "kaspin.db", null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS tb_barang(id integer primary key autoincrement, kode_barang TEXT, nama_barang TEXT, harga_jual REAL, harga_beli REAL)");

        String CREATE_MANAJEMEN_STOK_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_MANAJEMEN_STOK + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_CODE_BARANG + " TEXT," + KEY_DATE + " TEXT," + KEY_STOKIN + " INTEGER,"
                + KEY_PURCHASE_PRICE + " INTEGER" + ")";
        db.execSQL(CREATE_MANAJEMEN_STOK_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_BARANG);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_MANAJEMEN_STOK);

        onCreate(sqLiteDatabase);

    }

    public void simpanBarang(BarangModel barangModel) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("kode_barang", barangModel.getKode_barang());
            contentValues.put("nama_barang", barangModel.getNama_barang());
            contentValues.put("harga_jual", barangModel.getHarga_jual());
            contentValues.put("harga_beli", barangModel.getHarga_beli());

            db.insert(TABLE_BARANG, null, contentValues);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    public int updateBarang(BarangModel barangModel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("kode_barang", barangModel.getKode_barang());
        contentValues.put("nama_barang", barangModel.getNama_barang());
        contentValues.put("harga_jual", barangModel.getHarga_jual());
        contentValues.put("harga_beli", barangModel.getHarga_beli());

        // updating row
        return db.update(TABLE_BARANG, contentValues, "id" + " = ?",
                new String[] { String.valueOf(barangModel.getId()) });
    }

    public void deleteBarang(BarangModel barangModel) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_BARANG, "id" + " = ?",
                new String[] { String.valueOf(barangModel.getId()) });
        db.close();
    }


    public List<BarangModel> getAllBarang() {
        List<BarangModel> barangList = new ArrayList<BarangModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_BARANG;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                BarangModel barangModel = new BarangModel();
                barangModel.setId(Integer.parseInt(cursor.getString(0)));
                barangModel.setKode_barang(cursor.getString(1));
                barangModel.setNama_barang(cursor.getString(2));
                barangModel.setHarga_jual(Double.parseDouble(cursor.getString(3)));

                barangList.add(barangModel);
            } while (cursor.moveToNext());
        }

        return barangList;
    }

    //save stok
    public void saveStok(StokModel stokModel) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_CODE_BARANG, stokModel.getKode_barang());
            contentValues.put(KEY_DATE, stokModel.getTanggal());
            contentValues.put(KEY_STOKIN, stokModel.getStok_masuk());
            contentValues.put(KEY_PURCHASE_PRICE, stokModel.getHarga_jual());

            db.insert(TABLE_MANAJEMEN_STOK, null, contentValues);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    public StokModel getStokByCode(String kode_barang) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_MANAJEMEN_STOK, new String[] { KEY_ID,KEY_CODE_BARANG,
                        KEY_DATE, KEY_STOKIN, KEY_PURCHASE_PRICE }, KEY_CODE_BARANG + "=?",
                new String[] { String.valueOf(kode_barang) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        StokModel stokModel = new StokModel(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), Integer.parseInt(cursor.getString(3)),Integer.parseInt(cursor.getString(3)));
        return stokModel;
    }

    public List<StokModel> getListStokByCode(String kode_barang){
        List<StokModel> stokList = new ArrayList<StokModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_MANAJEMEN_STOK + " WHERE "+KEY_CODE_BARANG+" like '%"+kode_barang+"%'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                StokModel stokModel = new StokModel();
                stokModel.setId(Integer.parseInt(cursor.getString(0)));
                stokModel.setKode_barang(cursor.getString(1));
                stokModel.setTanggal(cursor.getString(2));
                stokModel.setStok_masuk(Integer.parseInt(cursor.getString(3)));
                stokModel.setHarga_jual(Integer.parseInt(cursor.getString(4)));

                stokList.add(stokModel);
            } while (cursor.moveToNext());
        }

        return stokList;
    }

    public List<StokModel> getAllStok() {
        List<StokModel> stokList = new ArrayList<StokModel>();
        String selectQuery = "SELECT  * FROM " + TABLE_MANAJEMEN_STOK;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                StokModel stokModel = new StokModel();
                stokModel.setId(Integer.parseInt(cursor.getString(0)));
                stokModel.setKode_barang(cursor.getString(1));
                stokModel.setTanggal(cursor.getString(2));
                stokModel.setStok_masuk(Integer.parseInt(cursor.getString(3)));
                stokModel.setHarga_jual(Integer.parseInt(cursor.getString(4)));

                stokList.add(stokModel);
            } while (cursor.moveToNext());
        }

        return stokList;
    }
}
