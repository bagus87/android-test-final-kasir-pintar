package com.example.kaspinfinaltes.helper;

import java.text.NumberFormat;
import java.util.Locale;

public class Tools {
    public static String formatRupiah(Double price){
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        return formatRupiah.format((double)price);
    }
}
