package com.example.kaspinfinaltes.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kaspinfinaltes.R;
import com.example.kaspinfinaltes.helper.Tools;
import com.example.kaspinfinaltes.model.BarangModel;
import com.example.kaspinfinaltes.model.StokModel;
import com.example.kaspinfinaltes.viewModel.StokViewModel;

import java.util.List;

public class BarangAdapter extends RecyclerView.Adapter<BarangAdapter.MenuViewHolder>{

    private static BarangAdapter.CellClickListener clickListener;
    private List<BarangModel> data;
    private Context mContext;

    private Integer size = 0;
    private int pos = 999;

    private StokViewModel stokViewModel;

    private LifecycleOwner lifecycleOwner;
    private FragmentActivity activity;

    public BarangAdapter(List<BarangModel> dataMenus,FragmentActivity activity, Context context,StokViewModel stokViewModel, BarangAdapter.CellClickListener listener) {
        this.data = dataMenus;
        this.mContext = context;
        this.stokViewModel = stokViewModel;
        this.activity = activity;
        clickListener = listener;
    }

    @NonNull
    @Override
    public BarangAdapter.MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BarangAdapter.MenuViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_barang, parent, false
                )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull BarangAdapter.MenuViewHolder holder, int position) {
        holder.setOnBoardingData(data.get(position), position);
        holder.itemView.setOnClickListener(v ->
                clickListener.selectBarang(data.get(position))
        );
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MenuViewHolder extends RecyclerView.ViewHolder{

        private final TextView tvName, tvCode, tvOriPrice, tvPrice, tvStok;
        private int purchasePrice = 0;

        MenuViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvTitle);

            tvCode = itemView.findViewById(R.id.tvCode);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvOriPrice = itemView.findViewById(R.id.tvOriginalPrice);
            tvStok = itemView.findViewById(R.id.tvStok);

        }

        void setOnBoardingData(BarangModel data, int position){
            tvName.setText(data.getNama_barang());
            tvCode.setText(data.getKode_barang());
            tvPrice.setText(Tools.formatRupiah(data.getHarga_jual()));

            if (stokViewModel.getListStokByCode(data.getKode_barang()).isEmpty()){
                purchasePrice = 0;
                tvStok.setText("0");
            } else {
                int stok = 0;
                for(StokModel dataStok: stokViewModel.getListStokByCode(data.getKode_barang())){
                    stok = stok + dataStok.getStok_masuk();
                    purchasePrice = dataStok.getHarga_jual();
                }

                tvStok.setText(String.valueOf(stok));
            }

            tvOriPrice.setText("Harga dasar " + Tools.formatRupiah(Double.valueOf(purchasePrice)));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public interface CellClickListener {
        void selectBarang(BarangModel data);
    }
}
