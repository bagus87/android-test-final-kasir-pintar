package com.example.kaspinfinaltes.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.kaspinfinaltes.model.BarangModel;
import com.example.kaspinfinaltes.repository.BarangRepository;

import java.util.List;

public class BarangViewModel extends AndroidViewModel {

    MutableLiveData<List<BarangModel>> barangModels = new MutableLiveData<>();

    BarangRepository barangRepository;

    public BarangViewModel(@NonNull Application application) {
        super(application);

        barangRepository = new BarangRepository(application);
    }

    public void simpanBarang(BarangModel barangModel) {
        barangRepository.simpanBarang(barangModel);
    }

    public void updateBarang(BarangModel barangModel) {
        barangRepository.updateBarang(barangModel);
    }

    public LiveData<List<BarangModel>> fetchBarang() {
        barangModels.setValue(barangRepository.fetchBarang());
        return barangModels;
    }
}
