package com.example.kaspinfinaltes.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.kaspinfinaltes.model.StokModel;
import com.example.kaspinfinaltes.repository.StokRepository;

import java.util.List;

public class StokViewModel extends AndroidViewModel {

    MutableLiveData<List<StokModel>> stokModels = new MutableLiveData<>();

    StokRepository stokRepository;

    public StokViewModel(@NonNull Application application) {
        super(application);

        stokRepository = new StokRepository(application);
    }

    public void saveStok(StokModel stokModel) {
        stokRepository.saveStok(stokModel);
    }

    public StokModel getStokByCode(String kode_barang) {
        return stokRepository.getStokByCode(kode_barang);
    }

    public LiveData<List<StokModel>> fetchStok() {
        stokModels.setValue(stokRepository.fetchStok());
        return stokModels;
    }

    public List<StokModel> getListStokByCode(String kode_barang) {
//        stokModels.setValue(stokRepository.getListStokByCode(kode_barang));
        return stokRepository.getListStokByCode(kode_barang);
    }
}
