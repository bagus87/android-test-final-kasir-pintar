package com.example.kaspinfinaltes.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BarangModel implements Parcelable {
    int id;
    String kode_barang;
    String nama_barang;
    double harga_jual;
    double harga_beli;

    public BarangModel(){}

    public BarangModel(int id,String kode_barang, String nama_barang, double harga_jual, double harga_beli) {
        this.id = id;
        this.kode_barang = kode_barang;
        this.nama_barang = nama_barang;
        this.harga_jual = harga_jual;
        this.harga_beli = harga_beli;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public double getHarga_jual() {
        return harga_jual;
    }

    public void setHarga_jual(double harga_jual) {
        this.harga_jual = harga_jual;
    }

    public double getHarga_beli() {
        return harga_beli;
    }

    public void setHarga_beli(double harga_beli) {
        this.harga_beli = harga_beli;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.kode_barang);
        dest.writeString(this.nama_barang);
        dest.writeDouble(this.harga_jual);
        dest.writeDouble(this.harga_beli);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readInt();
        this.kode_barang = source.readString();
        this.nama_barang = source.readString();
        this.harga_jual = source.readDouble();
        this.harga_beli = source.readDouble();
    }

    protected BarangModel(Parcel in) {
        this.id = in.readInt();
        this.kode_barang = in.readString();
        this.nama_barang = in.readString();
        this.harga_jual = in.readDouble();
        this.harga_beli = in.readDouble();
    }

    public static final Parcelable.Creator<BarangModel> CREATOR = new Parcelable.Creator<BarangModel>() {
        @Override
        public BarangModel createFromParcel(Parcel source) {
            return new BarangModel(source);
        }

        @Override
        public BarangModel[] newArray(int size) {
            return new BarangModel[size];
        }
    };
}
