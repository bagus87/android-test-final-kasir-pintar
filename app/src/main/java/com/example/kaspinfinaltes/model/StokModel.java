package com.example.kaspinfinaltes.model;

public class StokModel {
    int id;
    String kode_barang;
    String tanggal;
    int stok_masuk;
    int harga_jual;

    public StokModel(){}

    public StokModel(int id, String kode_barang, String tanggal, int stok_masuk, int harga_jual) {
        this.id = id;
        this.kode_barang = kode_barang;
        this.tanggal = tanggal;
        this.stok_masuk = stok_masuk;
        this.harga_jual = harga_jual;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public int getStok_masuk() {
        return stok_masuk;
    }

    public void setStok_masuk(int stok_masuk) {
        this.stok_masuk = stok_masuk;
    }

    public int getHarga_jual() {
        return harga_jual;
    }

    public void setHarga_jual(int harga_jual) {
        this.harga_jual = harga_jual;
    }
}
