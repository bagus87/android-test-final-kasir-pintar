package com.example.kaspinfinaltes.view.base;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public abstract class BaseFragment extends Fragment {
    public Toast toast;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void toast(String message){
        if (message != null && !message.isEmpty()){
            if (toast != null) toast.cancel();
            toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
