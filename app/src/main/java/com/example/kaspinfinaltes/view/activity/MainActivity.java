package com.example.kaspinfinaltes.view.activity;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.kaspinfinaltes.R;
import com.example.kaspinfinaltes.databinding.ActivityMainBinding;
import com.example.kaspinfinaltes.view.fragment.HomeFragment;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

    }

    public void onBackPressed() {
        if(getForegroundFragment() instanceof HomeFragment){
//            dialogLogout();
        } else {
            super.onBackPressed();
        }
    }

    public Fragment getForegroundFragment() {
        Fragment navHostFragment = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment_content_home);
        return navHostFragment.getChildFragmentManager().getFragments().get(0);
    }

}