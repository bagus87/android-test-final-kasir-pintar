package com.example.kaspinfinaltes.view.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.example.kaspinfinaltes.databinding.FragmentAddBarangBinding;
import com.example.kaspinfinaltes.model.BarangModel;
import com.example.kaspinfinaltes.view.base.BaseFragment;
import com.example.kaspinfinaltes.viewModel.BarangViewModel;
import com.example.kaspinfinaltes.viewModel.StokViewModel;

import java.text.NumberFormat;

public class AddBarangFragment extends BaseFragment {
    private FragmentAddBarangBinding binding;

    private View root;

    BarangViewModel barangViewModel;
    StokViewModel stokViewModel;

    int price_sell = 0;

    private String current = "";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentAddBarangBinding.inflate(inflater, container, false);
        root = binding.getRoot();

        barangViewModel = new ViewModelProvider(this).get(BarangViewModel.class);
        stokViewModel = new ViewModelProvider(this).get(StokViewModel.class);

        setView();
        setListener();

        return root;
    }

    private void setView(){
        binding.toolbar.ivBack.setVisibility(View.VISIBLE);
        binding.toolbar.tvTitleToolbar.setText("Tambah barang");
    }

    private void setListener(){
        binding.edtPrice.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals(current)) {
                    binding.edtPrice.removeTextChangedListener(this);

                    String replaceable = String.format("[%s,.\\s]", NumberFormat.getCurrencyInstance().getCurrency().getSymbol());
                    String cleanString = s.toString().replaceAll(replaceable, "");

                    if (cleanString.isEmpty()){
                        price_sell = 0;
                    } else {
                        price_sell = Integer.parseInt(cleanString);
                    }

                    double parsed;
                    try {
                        parsed = Double.parseDouble(cleanString);
                    } catch (NumberFormatException e) {
                        parsed = 0.00;
                    }
                    NumberFormat formatter = NumberFormat.getCurrencyInstance();
                    formatter.setMaximumFractionDigits(0);
                    String formatted = formatter.format((parsed));

                    current = formatted;
                    binding.edtPrice.setText(formatted);
                    binding.edtPrice.setSelection(formatted.length());
                    binding.edtPrice.addTextChangedListener(this);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });


        binding.toolbar.ivBack.setOnClickListener(v->{
            getActivity().onBackPressed();
        });

        binding.btnSave.setOnClickListener(V->{
            String kode_barang = binding.edtCodeBarang.getText().toString();
            String nama_barang = binding.edtNameBarang.getText().toString();
            String hargaJual = binding.edtPrice.getText().toString();

            if (kode_barang.isEmpty()){
                toast("Kode barang harus diisi!");
                return;
            }
            if (nama_barang.isEmpty()){
                toast("Nama barang harus diisi!");
                return;
            }
            if (hargaJual.isEmpty()){
                toast("Harga jual harus diisi!");
                return;
            }

            if (price_sell == 0){
                toast("Harga jual harus diisi!");
                return;
            }

//            double harga_jual = Double.parseDouble(binding.edtPrice.getText().toString());

            BarangModel barangModel = new BarangModel(0, kode_barang, nama_barang, price_sell, 0);
            barangViewModel.simpanBarang(barangModel);

            toast("Barang berhasil ditambahkan");
            requireActivity().onBackPressed();
        });
    }
}
