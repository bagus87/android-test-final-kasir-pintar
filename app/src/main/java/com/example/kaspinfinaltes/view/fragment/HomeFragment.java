package com.example.kaspinfinaltes.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.example.kaspinfinaltes.R;
import com.example.kaspinfinaltes.adapter.BarangAdapter;
import com.example.kaspinfinaltes.databinding.FragmentHomeBinding;
import com.example.kaspinfinaltes.model.BarangModel;
import com.example.kaspinfinaltes.view.base.BaseFragment;
import com.example.kaspinfinaltes.viewModel.BarangViewModel;
import com.example.kaspinfinaltes.viewModel.StokViewModel;

public class HomeFragment extends BaseFragment implements BarangAdapter.CellClickListener {
    private FragmentHomeBinding binding;

    private View root;

    BarangViewModel barangViewModel;
    StokViewModel stokViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        root = binding.getRoot();

        barangViewModel = new ViewModelProvider(this).get(BarangViewModel.class);
        stokViewModel = new ViewModelProvider(this).get(StokViewModel.class);

        setView();
        setListener();

        return root;
    }

    private void setView(){
        binding.toolbar.tvTitleToolbar.setText("Barang");

        barangViewModel.fetchBarang().observe(getViewLifecycleOwner(), list -> {
            if (list.size() == 0){
                binding.viewFlipper.setDisplayedChild(1);
            } else {
                binding.rvBarang.setAdapter(new BarangAdapter(list, requireActivity() ,getContext(),stokViewModel,this));
                binding.viewFlipper.setDisplayedChild(0);
            }
        });
    }

    private void setListener(){
        binding.btnAdd.setOnClickListener(v->{
            Navigation.findNavController(root).navigate(R.id.addBarangFragment);
        });
    }

    @Override
    public void selectBarang(BarangModel data) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("dataBarang" , data);
        Navigation.findNavController(root).navigate(R.id.detailBarangFragment, bundle);
    }
}
