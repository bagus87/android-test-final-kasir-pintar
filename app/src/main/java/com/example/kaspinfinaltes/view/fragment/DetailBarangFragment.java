package com.example.kaspinfinaltes.view.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import com.example.kaspinfinaltes.R;
import com.example.kaspinfinaltes.databinding.FragmentDetailBarangBinding;
import com.example.kaspinfinaltes.model.BarangModel;
import com.example.kaspinfinaltes.model.StokModel;
import com.example.kaspinfinaltes.view.base.BaseFragment;
import com.example.kaspinfinaltes.viewModel.BarangViewModel;
import com.example.kaspinfinaltes.viewModel.StokViewModel;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DetailBarangFragment extends BaseFragment {
    private FragmentDetailBarangBinding binding;

    private View root;
    StokViewModel stokViewModel;
    BarangViewModel barangViewModel;

    String kode_barang = "";

    BarangModel dataBarang;

    int price_sell = 0;
    int price_purchase = 0;

    private int mYear, mMonth, mDay, mHour, mMinute;
    private String current, currentpurchase, selectDate = "";
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentDetailBarangBinding.inflate(inflater, container, false);
        root = binding.getRoot();

        assert getArguments() != null;
        dataBarang = getArguments().getParcelable("dataBarang");

        barangViewModel = new ViewModelProvider(this).get(BarangViewModel.class);
        stokViewModel = new ViewModelProvider(this).get(StokViewModel.class);

        setView();
        setListener();

        return root;
    }

    private void setView(){
        binding.toolbar.tvTitleToolbar.setText("Detail barang");
        binding.toolbar.ivBack.setVisibility(View.VISIBLE);

        if (dataBarang != null){
            kode_barang = dataBarang.getKode_barang();

            binding.edtCodeBarang.setText(dataBarang.getKode_barang());
            binding.edtCodeBarang.setEnabled(false);

            binding.edtNameBarang.setText(dataBarang.getNama_barang());

//            binding.edtPrice.setText(String.valueOf(dataBarang.getHarga_jual()));

            String cleanString = String.valueOf((int) dataBarang.getHarga_jual());

            if (cleanString.isEmpty()){
                price_sell = 0;
            } else {
                price_sell = Integer.parseInt(cleanString);
            }

            double parsed;
            try {
                parsed = Double.parseDouble(cleanString);
            } catch (NumberFormatException e) {
                parsed = 0.00;
            }
            NumberFormat formatter = NumberFormat.getCurrencyInstance();
            formatter.setMaximumFractionDigits(0);
            String formatted = formatter.format((parsed));

            current = formatted;
            binding.edtPrice.setText(formatted);
        }
    }

    private void setListener(){
        binding.toolbar.ivBack.setOnClickListener(v->{
            requireActivity().onBackPressed();
        });

        binding.btnAddStok.setOnClickListener(v->{
            showDialogAddStok();
        });

        binding.edtPrice.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals(current)) {
                    binding.edtPrice.removeTextChangedListener(this);

                    String replaceable = String.format("[%s,.\\s]", NumberFormat.getCurrencyInstance().getCurrency().getSymbol());
                    String cleanString = s.toString().replaceAll(replaceable, "");

                    if (cleanString.isEmpty()){
                        price_sell = 0;
                    } else {
                        price_sell = Integer.parseInt(cleanString);
                    }

                    double parsed;
                    try {
                        parsed = Double.parseDouble(cleanString);
                    } catch (NumberFormatException e) {
                        parsed = 0.00;
                    }
                    NumberFormat formatter = NumberFormat.getCurrencyInstance();
                    formatter.setMaximumFractionDigits(0);
                    String formatted = formatter.format((parsed));

                    current = formatted;
                    binding.edtPrice.setText(formatted);
                    binding.edtPrice.setSelection(formatted.length());
                    binding.edtPrice.addTextChangedListener(this);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        binding.btnSave.setOnClickListener(V->{
            String kode_barang = binding.edtCodeBarang.getText().toString();
            String nama_barang = binding.edtNameBarang.getText().toString();
            String hargaJual = binding.edtPrice.getText().toString();

            if (kode_barang.isEmpty()){
                toast("Kode barang harus diisi!");
                return;
            }
            if (nama_barang.isEmpty()){
                toast("Nama barang harus diisi!");
                return;
            }
            if (hargaJual.isEmpty()){
                toast("Harga jual harus diisi!");
                return;
            }

            if (price_sell == 0){
                toast("Harga jual harus diisi!");
                return;
            }

//            double harga_jual = Double.parseDouble(binding.edtPrice.getText().toString());

            BarangModel barangModel = new BarangModel(dataBarang.getId(), kode_barang, nama_barang, price_sell, 0);
            barangViewModel.updateBarang(barangModel);

            toast("Barang berhasil diupdate");
            requireActivity().onBackPressed();
        });
    }

    private void showDialogAddStok(){
        Dialog pDialog = new Dialog(requireContext(), R.style.DialogLight);
        pDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pDialog.setContentView(R.layout.dialog_add_stok);
        pDialog.setCancelable(false);

        TextView tvDate = pDialog.findViewById(R.id.tvDate);

        tvDate.setText(datenow());
        selectDate = datenow();

        EditText edtStokIn = pDialog.findViewById(R.id.edtStokIn);
        EditText edtPurchasePrice = pDialog.findViewById(R.id.edtPurchasePrice);

        Button btnAdd = pDialog.findViewById(R.id.btnAdd);
        Button btnCancel = pDialog.findViewById(R.id.btnCancel);

        ConstraintLayout clDate = pDialog.findViewById(R.id.clDate);

        Point size = new Point();
        WindowManager wm = (WindowManager) requireContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        display.getSize(size);
        int mWidth = size.x;

        Window window = pDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.x = 0;
        wlp.y = 0;
        wlp.width = mWidth;
        window.setAttributes(wlp);
        pDialog.show();

        btnCancel.setOnClickListener(v->{
            pDialog.dismiss();
        });

        clDate.setOnClickListener(v->{

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {

//                            dd/MM/yyyy hh:mm:ss
                            selectDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                            final Calendar c = Calendar.getInstance();
                            mHour = c.get(Calendar.HOUR_OF_DAY);
                            mMinute = c.get(Calendar.MINUTE);
//                            int mSeconds = c.get(Calendar.SECOND);

                            // Launch Time Picker Dialog
                            TimePickerDialog timePickerDialog = new TimePickerDialog(requireActivity(),
                                    new TimePickerDialog.OnTimeSetListener() {

                                        @Override
                                        public void onTimeSet(TimePicker view, int hourOfDay,
                                                              int minute) {

                                            String hours = "";
                                            if (String.valueOf(hourOfDay).length() == 1){
                                                hours = "0" + String.valueOf(hourOfDay);
                                            } else {
                                                hours = String.valueOf(hourOfDay);
                                            }
                                            String minutes = "";
                                            if (String.valueOf(minute).length() == 1){
                                                minutes = "0" + String.valueOf(minute);
                                            } else {
                                                minutes = String.valueOf(minute);
                                            }

                                            selectDate = selectDate + " " + hours + ":" + minutes;

                                            tvDate.setText(selectDate);
                                        }
                                    }, mHour, mMinute, false);
                            timePickerDialog.show();
                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();


        });

        edtPurchasePrice.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals(currentpurchase)) {
                    edtPurchasePrice.removeTextChangedListener(this);

                    String replaceable = String.format("[%s,.\\s]", NumberFormat.getCurrencyInstance().getCurrency().getSymbol());
                    String cleanString = s.toString().replaceAll(replaceable, "");

                    if (cleanString.isEmpty()){
                        price_purchase = 0;
                    } else {
                        price_purchase = Integer.parseInt(cleanString);
                    }

                    double parsed;
                    try {
                        parsed = Double.parseDouble(cleanString);
                    } catch (NumberFormatException e) {
                        parsed = 0.00;
                    }
                    NumberFormat formatter = NumberFormat.getCurrencyInstance();
                    formatter.setMaximumFractionDigits(0);
                    String formatted = formatter.format((parsed));

                    currentpurchase = formatted;
                    edtPurchasePrice.setText(formatted);
                    edtPurchasePrice.setSelection(formatted.length());
                    edtPurchasePrice.addTextChangedListener(this);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });


        btnAdd.setOnClickListener(v->{
            String stokMasuk = edtStokIn.getText().toString();
            String purchasePrice = edtPurchasePrice.getText().toString();
            String date = tvDate.getText().toString();

            if (stokMasuk.isEmpty()){
                toast("Jumlah stok harus diisi");
                return;
            }

            if (purchasePrice.isEmpty()){
                toast("Harga beli harus diisi");
                return;
            }

            if (price_purchase == 0){
                toast("Harga beli harus diisi!");
                return;
            }

            StokModel dataStok = new StokModel(
                    0,
                    kode_barang,
                    date,
                    Integer.parseInt(stokMasuk),
                    price_purchase
            );
            stokViewModel.saveStok(dataStok);

            price_purchase = 0;
            pDialog.cancel();

            toast("Stok berhasil ditambahkan");
//            requireActivity().onBackPressed();
        });
    }

    private String datenow(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        Date currentDate = new Date();
        String currentDateString = dateFormat.format(currentDate);

        return currentDateString;
    }
}
