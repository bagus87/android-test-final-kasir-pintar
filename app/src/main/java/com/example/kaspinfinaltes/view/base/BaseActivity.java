package com.example.kaspinfinaltes.view.base;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {
    public Toast toast;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void toast(String message){
        if (message != null && !message.isEmpty()){
            if (toast != null) toast.cancel();
            toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
