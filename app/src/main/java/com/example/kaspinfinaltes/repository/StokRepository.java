package com.example.kaspinfinaltes.repository;

import android.app.Application;

import com.example.kaspinfinaltes.db.DatabaseHelper;
import com.example.kaspinfinaltes.model.BarangModel;
import com.example.kaspinfinaltes.model.StokModel;

import java.util.List;

public class StokRepository {

    DatabaseHelper dbHelper;

    public StokRepository(Application app) {
        dbHelper = new DatabaseHelper(app);
    }

    public void saveStok(StokModel stokModel) {
        dbHelper.saveStok(stokModel);
    }

    public StokModel getStokByCode(String kode_barang) {
        StokModel dataStok = new StokModel();
        dataStok = dbHelper.getStokByCode(kode_barang);
        return dataStok;
    }

    public List<StokModel> fetchStok() {
        return dbHelper.getAllStok();
    }

    public List<StokModel> getListStokByCode(String kode_barang) {
        return dbHelper.getListStokByCode(kode_barang);
    }
}