package com.example.kaspinfinaltes.repository;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.kaspinfinaltes.db.DatabaseHelper;
import com.example.kaspinfinaltes.model.BarangModel;

import java.util.List;

public class BarangRepository {

    DatabaseHelper dbHelper;

    public BarangRepository(Application app) {
        dbHelper = new DatabaseHelper(app);
    }

    public void simpanBarang(BarangModel barangModel) {
        dbHelper.simpanBarang(barangModel);
    }

    public void updateBarang(BarangModel barangModel) {
        dbHelper.updateBarang(barangModel);
    }

    public List<BarangModel> fetchBarang() {
        return dbHelper.getAllBarang();
    }
}